/* 
    * @desc - constant value defintions
*/

// * @desc - possible type of messages incoming from socket connections
export enum MessageTypes {
  joinChat = "joinChat",
  chatAll = "chatAll",
  chatPersonal = "chatPersonal",
  chatGroup = "chatGroup",
}
