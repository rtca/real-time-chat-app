/*
 * @desc - holds functions related to websocket connections and messaging
 */
import { MessageTypes } from "./constants.ts";
import { ChatSocketConnection } from "./interfaces.ts";

import {
  isWebSocketCloseEvent,
  isWebSocketPingEvent,
  WebSocket,
} from "https://deno.land/std@0.88.0/ws/mod.ts";

// import default logger library from std
import * as log from "https://deno.land/std@0.88.0/log/mod.ts";
import { ConnectionStore } from "./ConnectionStore.ts";

// * @desc - sends message to all active socket connection, includes sender
export const messageAllSockets = (
  sender: ChatSocketConnection,
  connections: Array<ChatSocketConnection>,
  message: string,
  participants: Array<string>,
): void => {
  // create message object
  const messageObject = {
    message: message,
    type: "chatAll",
    name: sender.name,
    participants,
  };

  // send message to all sockets
  connections.forEach((socket) =>
    socket.websocket.send(JSON.stringify(messageObject))
  );
};

// * @desc - sends message to all other sockets, excludes sender
export const messageOtherSockets = (
  sender: ChatSocketConnection,
  connections: Array<ChatSocketConnection>,
  message: string,
  participants: Array<string>,
): void => {
  // for each socket that isn't the sender, send a message
  connections.forEach((socket: ChatSocketConnection) => {
    if (socket.websocket != sender.websocket) {
      socket.websocket.send(message);
    } else {
      log.warning(
        "--- socket and sender are the same. This does not matter if it is a 'joinChat' message type",
      );
    }
  });
};

// * @desc - sends a message letting other users know you have joined a chat, calls messageOtherSockets
export const joinChat = (
  sender: ChatSocketConnection,
  socketConnections: Array<ChatSocketConnection>,
  participants: Array<string>,
): void => {
  // destructure from sender
  const { name, websocket } = sender;

  // message object to stringify and send to sockets
  const messageObject = {
    message: `${name} has joined the chat.`,
    type: "joinChat",
    name,
    participants,
  };

  // send join message to others
  messageOtherSockets(
    sender,
    socketConnections,
    JSON.stringify(messageObject),
    participants,
  );

  // send yourself a confirmation
  websocket.send(
    JSON.stringify({
      message: "You joined the chat.",
      type: "joinChat",
      name,
      participants,
    }),
  );
};

// * @desc - broadcast the active participant list to all current sockets
export const broadcastParticipantList = (
  sender: ChatSocketConnection,
  socketConnections: Array<ChatSocketConnection>,
  name: string,
  participants: Array<string>,
): void => {
  messageAllSockets(sender, socketConnections, name, participants);
};

// * @desc - handles new websocket connection
export const handleNewSocketConnection = async (
  ws: WebSocket,
  connectionStore: ConnectionStore,
): Promise<void> => {
  log.info("--- websocket connection established");

  // log events that come in over websocket connection
  for await (const event of ws) {
    // check the type of event coming from client
    if (typeof event === "string") {
      // text message.
      console.log(JSON.parse(event));

      // parse string message as JSON and check the type to determine action to take
      const messageObj = JSON.parse(event);

      // destructure message and type from message object
      const { name, message, type } = messageObj;

      // create object fashioned as ChatSocketConnection interfaces
      const chatSocketConnection = {
        name,
        websocket: ws,
      };

      // add the current connection to the store and update the connections list
      connectionStore.addConnection(chatSocketConnection);

      // destructure connections and participants from store
      const { connections, participants } = connectionStore;

      // check message type
      switch (type) {
        case MessageTypes.joinChat:
          // join the chat
          joinChat(
            chatSocketConnection,
            connections,
            participants,
          );
          break;
        case MessageTypes.chatAll:
          // check to see if we're allow to message ourself
          if (!Deno.env.get("SEND_SELF_MESSAGE")) {
            messageOtherSockets(
              chatSocketConnection,
              connections,
              message,
              participants,
            );
          } else {
            messageAllSockets(
              chatSocketConnection,
              connections,
              message,
              participants,
            );
          }
          break;
        default:
          break;
      }
    } else if (event instanceof Uint8Array) {
      // binary message.
      console.log("ws:Binary", event);
    } else if (isWebSocketPingEvent(event)) {
      const [, body] = event;
      // ping.
      console.log("ws:Ping", body);
    } else if (isWebSocketCloseEvent(event)) {
      // close.
      const { code, reason } = event;
      console.log("ws:Close", code, reason, event);

      // if the sockect is found, remove it from the list of active connections
      removeFromSocketConnections(ws, connectionStore);
    }
  }
};

// * @desc - remove single websocket connection from connections list
export const removeFromSocketConnections = async (
  currentSocket: WebSocket,
  connectionStore: ConnectionStore,
): Promise<void> => {
  // get the current socket, false if socket is not found
  const currentConnection: ChatSocketConnection | false = getCurrentSocket(
    currentSocket,
    connectionStore,
  );

  // if the current socket exists in the list of socket connections
  if (currentConnection) {
    // destructure name from currentConnection
    const { name } = currentConnection;

    // remove from store array and dictionary
    connectionStore.removeConnection(currentConnection);
    connectionStore.removeFromDictionary(currentConnection);

    // destructure connections and participants from store
    const { connections, participants } = connectionStore;

    // define left chat message
    const message = `${name} has left the chat`;

    // message object to be sent to messageOtherSockets
    const messageObject = {
      message,
      type: "leaveChat",
      name,
      participants,
    };

    // let other sockets know current participant left
    messageOtherSockets(
      currentConnection,
      connections,
      JSON.stringify(messageObject),
      participants,
    );

    // log that the connection has been removed on the backend
    log.info(
      ` --- ${name}'s connection removed from connections list @ ${new Date(
        Date.now(),
      )}`,
    );
  } else {
    log.warning(` --- Socket could not be located.`);
  }
};

// * @desc - gets the current socket from the list of sockets in the store
export const getCurrentSocket = (
  websocket: WebSocket,
  connectionStore: ConnectionStore,
): ChatSocketConnection | false => {
  // destrructure connections from store
  const { connections } = connectionStore;

  // find the socket in the list which matches the given websocket
  const currentSocketConnection = connections.filter(
    (connection: ChatSocketConnection) => connection.websocket === websocket,
  )[0] || false;

  return currentSocketConnection;
};

// * @desc - gets the index of the current socket
export const getCurrentSocketIndex = (
  currentChatSocketConnection: ChatSocketConnection,
  connectionStore: ConnectionStore,
): number => {
  // get sockets from store and return index of provided chat socket connection
  const connections = connectionStore.connections;
  return connections.indexOf(currentChatSocketConnection);
};
