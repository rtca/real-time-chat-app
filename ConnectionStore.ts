/*
 * @desc - connection store class which helps store and manage connections for a chat room
 */

import { ChatSocketConnection } from "./interfaces.ts";
import { WebSocket } from "https://deno.land/std@0.88.0/ws/mod.ts";
import * as log from "https://deno.land/std@0.88.0/log/mod.ts";

// connection store class to help store connections and various functions related to connections
export class ConnectionStore {
  // constructor shorthand declarations with default values
  constructor(
    private _roomNumber: number = 0,
    private _connections: ChatSocketConnection[] = [],
    private _connectionDictionary: { [name: string]: WebSocket } = {},
    private _participants: string[] = [],
  ) {}

  // exposes the connections list
  get connections(): Array<ChatSocketConnection> {
    return this._connections;
  }

  // exposes the dictionary of sockets
  get connectionDictionary(): Object {
    return this._connectionDictionary;
  }

  // exposes the names of the participating connections
  get participants(): Array<string> {
    // make sure the participants list is up to date with the connections list
    if (this._participants.length != this.connections.length) {
      // sets the participants to the name keys of the dictionary or an empty string array if no connections
      this._participants = (this._connections.length > 0)
        ? Object.keys(this._connectionDictionary)
        : [] as string[];
    }
    return this._participants;
  }

  // adds a new chat socket connection to the list of active socket connections and adds the name to the dictionary of names for quick lookup
  addConnection(
    conn: ChatSocketConnection,
  ): void {
    if (this.isChatSocketConnection(conn)) {
      // check for the connection in the dictionary
      if (!(conn.name in this._connectionDictionary)) {
        // add the name and socket key value pair to dict object
        this._connectionDictionary[conn.name] = conn.websocket;

        // add new connection to connections array
        this._connections.push(conn);
      }
    } else {
      log.warning(`Connection could not be added, it's already present.`);
    }
  }

  // removes a connection from the list
  removeConnection(
    conn: ChatSocketConnection,
  ): void {
    // find the correct index
    const index = this._connections.indexOf(conn);

    // if the index is found splice the array at the index to remove it
    if (index > -1) {
      this._connections.splice(index, 1);
    }
  }

  // adds a connection to the dictionary so name lookup is possible
  addToDictionary(conn: ChatSocketConnection): void {
    this._connectionDictionary[conn.name] = conn.websocket;
  }

  // removes a name from the dictionary
  removeFromDictionary(conn: ChatSocketConnection): void {
    if (conn.name in this._connectionDictionary) {
      delete this._connectionDictionary[conn.name];
    }
  }

  // returns whether or not a given object is in the form of a ChatSocketConnection
  isChatSocketConnection(
    object: any,
  ): object is ChatSocketConnection {
    // check that there are only two properties and confirm their names
    return (Object.keys(object).length === 2) && (`name` in object) &&
      (`websocket` in object) && (typeof (object.name) === `string`) &&
      (typeof (object.websocket) === `object`);
  }
}
