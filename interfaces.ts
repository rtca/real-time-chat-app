/*
   * @desc - custom interface definitions
*/

// import WebSocket object from deno std
import { WebSocket } from "https://deno.land/std@0.88.0/ws/mod.ts";
import { ConnectionStore } from "./ConnectionStore.ts";

// * @desc - wrapper for websocket that pairs it with a name
export interface ChatSocketConnection {
  name: string;
  websocket: WebSocket;
}

// * @desc - chat room which unique room id and set of chatSocketConnections (the participants)
export interface ChatRoom {
  id: number;
  name: string;
  connections: ConnectionStore;
}

// * @desc - chat message object representing a singular chat message
export interface ChatMessage {
  id: number;
  roomId: number;
  name: string;
  date: Date;
  message: string;
}

// * @desc - chat log representing a list of individual chat messages
export interface ChatLog {
  id: number;
  logs: Array<ChatMessage>;
}

// * @desc - chat user object representing a participant in a chat room or rooms
export interface ChatUser {
  id: number;
  name: string;
  rooms: Array<number>;
}
