/*
   * @desc - custom type definitions
*/

// * @desc - wraps a socket connection to name it
export type ChatSocketConnection = {
  name: string;
  websocket: WebSocket;
};

// * @desc - chat room which unique room id and set of chatSocketConnections (the participants)
export type ChatRoom = {
  id: number;
  connections: Set<ChatSocketConnection>;
};

// * @desc - chat message object representing a singular chat message
export type ChatMessage = {
  id: number;
  roomId: number;
  name: string;
  date: Date;
  message: string;
};

// * @desc - chat log representing a list of individual chat messages
export type ChatLog = {
  id: number;
  logs: Array<ChatMessage>;
};

// * @desc - chat user object representing a participant in a chat room
export type ChatUser = {
  id: number;
  name: string;
  rooms: Array<number>;
};
