// import config autoloader
import "https://deno.land/x/dotenv/load.ts";

// import serve from denoland server.ts
import { serve } from "https://deno.land/std@0.88.0/http/server.ts";

// import default logger library from std
import * as log from "https://deno.land/std@0.88.0/log/mod.ts";

// import websocket methods
import {
  acceptable,
  acceptWebSocket,
} from "https://deno.land/std@0.88.0/ws/mod.ts";

import { handleNewSocketConnection } from "./websocketUtils.ts";

import { ConnectionStore } from "./ConnectionStore.ts";

// set port for connection - pull from env
const port = +((Deno.env.get("SERVER_PORT") || 8080));

// set hostname for connection
const hostname = "0.0.0.0";

// create http server and start it
const server = serve({ hostname, port });
log.info(`--- HTTP webserver running. Access it at: localhost:8080/`);

// setup new chat connection store to manage connections
const connectionStore = new ConnectionStore();

// wait for asyncIterator returned from server
for await (const req of server) {
  // check if the request is acceptable as a websocket req
  if (acceptable(req)) {
    // destructure required websocket components from request object
    const { conn, r: bufReader, w: bufWriter, headers } = req;

    //upgrade connection to websocket connection
    acceptWebSocket({
      conn,
      bufReader,
      bufWriter,
      headers,
    }).then(async (ws) => {
      // websocket connection successful
      handleNewSocketConnection(ws, connectionStore);
    }).catch(async (e) => {
      log.error(
        ` --- Cannot upgrade connection to websocket connection. Reason: ${e.message}`,
      );
      await req.respond({ status: 400 });
    });
  } else {
    // if a non websocket request comes in respond
    if (req.proto.indexOf("HTTP")) {
      log.info(` --- ${req.proto} connection`);
    }

    // check for http get request to home
    if (req.method === "GET" && req.url === "/") { // allow access to home
      // respond with headers and body
      req.respond({
        headers: new Headers({
          "content-type": "text/html",
        }),
        body: await Deno.open("./public/index.html"),
      });
    } else if (req.method === "GET" && req.url === "/style.css") { // allow access to styles
      req.respond({
        headers: new Headers({
          "content-type": "text/css",
        }),
        body: await Deno.open("./public/style.css"),
      });
    } else {
      // 404
      req.respond({ body: "not found", status: 404 });
    }
  }
}
